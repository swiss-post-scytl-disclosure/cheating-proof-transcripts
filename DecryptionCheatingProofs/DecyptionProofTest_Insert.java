// This file is produced to demonstrate a problem in the Sctyl/SwissPost decryption proof.  
// It implements a cheating decryption proof method described in a report by
// Sarah Jamie Lewis, Olivier Pereira and Vanessa Teague.
// This function should be copied in to 
// /scytl-cryptolib/cryptolib-proofs/src/test/java/com/scytl/cryptolib/proofs/service/DecryptionProofTest.java
// and run.

    @Test
    public final void deserializeProofFromJSONAndCheckOk()
            throws GeneralCryptoLibException {

        ProofsService proofsServiceForDefaultPolicy = new ProofsService();

        ElGamalPublicKey publicKey = ElGamalPublicKey.fromJson("{\"publicKey\":{\"zpSubgroup\":{\"g\":\"Ag==\",\"p\":\"AK+RIPEJgBcfry8YICwtmoutvw==\",\"q\":\"V8iQeITAC4/Xl4wQFhbNRdbf\"},\"elements\":[\"QaWCaQ9VT+HC/M3NIIdgtbzi\"]}}");
        ElGamalPrivateKey privateKey = ElGamalPrivateKey.fromJson("{\"privateKey\":{\"zpSubgroup\":{\"g\":\"Ag==\",\"p\":\"AK+RIPEJgBcfry8YICwtmoutvw==\",\"q\":\"V8iQeITAC4/Xl4wQFhbNRdbf\"},\"exponents\":[\"KBZ/CN6wjWmK2/UqfG9xMY4U\"]}}");
        BigInteger p = publicKey.getGroup().getP();
        BigInteger q = publicKey.getGroup().getQ();

        // Prove that the private key and public key correspond.

        System.out.println("Checking that private and public keys correspond.\n");
        ZpGroupElement testPubKey = publicKey.getGroup().getGenerator().exponentiate(privateKey.getKeys().get(0));
        if (!testPubKey.equals(publicKey.getKeys().get(0)) || !publicKey.getGroup().equals(privateKey.getGroup())) {
            System.out.println("ERROR: The public key does not match the private key.");
        }

        // Load the proof that a nonsense ciphertext actually decrypts to 7.

        List<ZpGroupElement> plainText7 = new ArrayList<ZpGroupElement>();
        plainText7.add(new ZpGroupElement(new BigInteger("7"),p,q));
        Proof deserializedProof7 = Proof.fromJson("{\"zkProof\":{\"hash\":\"AiJOnK6Ogb3rIOMEHeS8TrFp\",\"values\":[\"Vm/69W8YWjmzXYyeMQUtMgtO\"],\"q\":\"V8iQeITAC4/Xl4wQFhbNRdbf\"}}");
        Ciphertext cheatingCiphertext = new CiphertextImpl(new ZpGroupElement(new BigInteger("14006614111069520674417588732162224161732678"),p,q),
                                                           new ZpGroupElement(new BigInteger("12443487329224121281177020933874319855098423"),p,q));

        System.out.println("Verifying Proof 1");
        System.out.println("Claimed plaintext =  "+plainText7.get(0));

        // Verify Proof.

        Assert.assertTrue(proofsServiceForDefaultPolicy
                .createProofVerifierAPI(publicKey.getGroup()).verifyDecryptionProof(
                        publicKey, cheatingCiphertext, plainText7, deserializedProof7));

        // Print decryption

        ElGamalServiceAPI elGamalService = new ElGamalService();
        CryptoAPIElGamalDecrypter decrypter = elGamalService.createDecrypter(privateKey);
        System.out.println("True decryption   = "+decrypter.decrypt(new ElGamalComputationsValues(cheatingCiphertext.getGamma(), cheatingCiphertext.getPhis()), true));
        System.out.println("Proof verified.\n");

        // Load the proof that an encryption of 3 actually decrypts to nonsense.

        List<ZpGroupElement> plaintextNonsense = new ArrayList<ZpGroupElement>();
        plaintextNonsense.add(new ZpGroupElement(new BigInteger("2750132861347711778626352267580572009032361"),p,q));
        Proof deserializedProof3Nonsense = Proof.fromJson("{\"zkProof\":{\"hash\":\"Ta51IEr4v7yGHV1NFqQM9AUM\",\"values\":[\"R6g+q1MFEQfKaVnqxDq9Z5YK\"],\"q\":\"V8iQeITAC4/Xl4wQFhbNRdbf\"}}");
        Ciphertext cheatingCiphertext3 = new CiphertextImpl(new ZpGroupElement(new BigInteger("10457373340113899312563075688819286008051707"),p,q),
                                         new ZpGroupElement(new BigInteger("3688530922996676253768130087998100046112146"),p,q));

        System.out.println("Verifying Proof 2");
        System.out.println("Claimed plaintext =  "+plaintextNonsense.get(0));

        // Verify Proof

        Assert.assertTrue(proofsServiceForDefaultPolicy
                .createProofVerifierAPI(publicKey.getGroup()).verifyDecryptionProof(
                        publicKey, cheatingCiphertext3, plaintextNonsense, deserializedProof3Nonsense));

        // Print decryption
        System.out.println("True decryption =   "+decrypter.decrypt(new ElGamalComputationsValues(cheatingCiphertext3.getGamma(), cheatingCiphertext3.getPhis()), true));
        System.out.println("Proof verified.");


    }

